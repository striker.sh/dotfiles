[global]
    ### Display ###
    monitor = 0
    follow = mouse
    geometry = "300x50-0+20"
    indicate_hidden = yes
    shrink = no
    transparency = 0
    notification_height = 0
    separator_height = 2
    padding = 8
    horizontal_padding = 8
    frame_width = 3.
    frame_color = "#01a754"
    separator_color = frame
    sort = yes
    idle_threshold = 120

    ### Text ###

    font = monospace 8
    line_height = 0
    markup = full
    format = "<b>%s</b>\n%b"
    alignment = left
    show_age_threshold = 60
    word_wrap = yes
    ellipsize = middle
    ignore_newline = no
    stack_duplicates = true
    hide_duplicate_count = false
    show_indicators = yes

    ### Icons ###

    icon_position = left
    max_icon_size = 80
    icon_path = ~/.icons/wal/status/scalable:~/.icons/wal/devices/scalable
    ### History ###

    sticky_history = yes
    history_length = 20


    ### Misc/Advanced ###

    # dmenu path.
    dmenu = /usr/bin/rofi -dmenu -p dunst:

    # Browser for opening urls in context menu.
    browser = /usr/bin/firefox -new-tab

    # Always run rule-defined scripts, even if the notification is suppressed
    always_run_script = true

    # Define the title of the windows spawned by dunst
    title = Dunst

    # Define the class of the windows spawned by dunst
    class = Dunst

    # Print a notification on startup.
    # This is mainly for error detection, since dbus (re-)starts dunst
    # automatically after a crash.
    startup_notification = false

    # Manage dunst's desire for talking
    # Can be one of the following values:
    #  crit: Critical features. Dunst aborts
    #  warn: Only non-fatal warnings
    #  mesg: Important Messages
    #  info: all unimportant stuff
    # debug: all less than unimportant stuff
    verbosity = mesg

    # Define the corner radius of the notification window
    # in pixel size. If the radius is 0, you have no rounded
    # corners.
    # The radius will be automatically lowered if it exceeds half of the
    # notification height to avoid clipping text and/or icons.
    corner_radius = 0
    ### Legacy

    force_xinerama = false

    ### mouse

    mouse_left_click = close_current
    mouse_middle_click = do_action
    mouse_right_click = close_all

[experimental]
    per_monitor_dpi = false

[shortcuts]
    close = ctrl+space
    close_all = ctrl+shift+space
    history = ctrl+grave
    context = ctrl+shift+period

[urgency_low]
    background = "#282828"
    foreground = "#ebdbb2"
    frame_color = "#928374"
    timeout = 10

[urgency_normal]
    background = "#282828"
    foreground = "#ebdbb2"
    frame_color = "#98971a"
    timeout = 10

[urgency_critical]
    background = "#282828"
    foreground = "#ebdbb2"
    frame_color = "#cc241d"
    timeout = 0

# vim: ft=cfg
