#!/bin/sh
# generate color variable to be sourced from Xresources
RES_FILE="$HOME/.Xresources"
OUTPUT_DIR="$HOME/.config/env/"
OUTPUT_FILE="colors"
SED_BEGIN='s/^*\.\?\('
SED_END='\): *\(\#[0-9a-fA-f]\+\)$/\1="\2"/p'

EXTRACT_VALUE='background
foreground
cursorColor
color[0-9]'

sed_cmd=''
for val in $EXTRACT_VALUE; do
   sed_cmd="$sed_cmd -e ""'${SED_BEGIN}${val}${SED_END}'"
done

if [ ! -d "$OUTPUT_DIR" ]; then
  mkdir "$OUTPUT_DIR"
fi

eval sed -n "$sed_cmd" "$RES_FILE" > "${OUTPUT_DIR}/${OUTPUT_FILE}"
