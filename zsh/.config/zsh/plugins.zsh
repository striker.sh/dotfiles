### Added by Zinit's installer
ZINIT_INSTALL_DIR="$HOME/.config/zsh/.zinit"
if [[ ! -f "$ZINIT_INSTALL_DIR/bin/zinit.zsh" ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing DHARMA Initiative Plugin Manager (zdharma/zinit)…%f"
    mkdir -p "$ZINIT_INSTALL_DIR" && chmod g-rwX "$ZINIT_INSTALL_DIR"
    git clone https://github.com/zdharma/zinit "$ZINIT_INSTALL_DIR/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f" || \
        print -P "%F{160}▓▒░ The clone has failed.%f"
fi
source "$ZINIT_INSTALL_DIR/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zinit installer's chunk
### zinit plugins
zinit light zsh-users/zsh-autosuggestions
zinit light zdharma/fast-syntax-highlighting
