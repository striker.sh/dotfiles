case "$TERM" in
    st*)
     precmd () {
         vcs_info
         print -Pn "\e]0;%~\a"
     }
     preexec () {
         print -Pn "\e]0;$1\a"
     }
   ;;
 screen*|tmux*)
     precmd () {
         vcs_info
         print -Pn "\e]0;%~\a"
         tmux rename-window "zsh"
     }
     preexec () {
         print -Pn "\e]0;$1\a"
         tmux rename-window "${1% *}"
     }
   ;;
esac

