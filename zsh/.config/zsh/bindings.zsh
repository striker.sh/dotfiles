# vi keybindings
bindkey -v
export KEYTIMEOUT=1

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search


# up and down history without entering normal mode
bindkey '^k' up-line-or-beginning-search
bindkey '^j' down-line-or-beginning-search
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search

# select inside menu vim-like
bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^k' vi-up-line-or-history
bindkey -M menuselect '^l' vi-forward-char
bindkey -M menuselect '^j' vi-down-line-or-history

# backspace and ^h working even after
# returning from command mode
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char

# some emacs keybingds
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey -M viins '^a' beginning-of-line
bindkey -M viins '^e' end-of-line

# C-r for research
bindkey '^r' history-incremental-search-backward
bindkey '^s' history-incremental-search-forward

# edit command
bindkey -M vicmd '^e' edit-command-line

# edit command line
autoload edit-command-line
zle -N edit-command-line

# mode indicator for vim mode
function zle-line-init zle-keymap-select {
  NORMAL_MODE_PROMPT="%F{magenta}[%F{green}NORMAL%F{magenta}]%f"
  RPS1="${${KEYMAP/vicmd/$NORMAL_MODE_PROMPT}/(main|viins)/}$EPS1"
  zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

bindkey '^\\' autosuggest-accept

#text-objects
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
done

autoload -U select-bracketed
zle -N select-bracketed
for m in visual viopp; do
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $m $c select-bracketed
  done
done
