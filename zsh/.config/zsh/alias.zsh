LS_PARAM="-hN --color=auto --group-directories-first"
alias lynx='lynx -vikeys'
alias v="vim"
# Colorize commands when possible.
alias \
	ls="ls ${LS_PARAM}" \
  ll="ls -l ${LS_PARAM}" \
  la="ls -la ${LS_PARAM}" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi" \
  ip="ip --color" \
  ipb="ip --color --brief" \
  info="info --vi-keys"

alias trackma-curses='
  PID="$(pgrep -x trackma-curses)"
  [ -n "$PID" ] && echo "trackma-curses is already running (PID: $PID)" || trackma-curses'

f() {
    fff "$@"
    cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}
