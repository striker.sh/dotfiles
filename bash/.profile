#!/bin/sh
# MPD daemon start (if no other user instance exists)
. /etc/profile
[ ! -f "$HOME/.config/mpd/pid" ] && mpd
# start ssh-agent if not already started
[ -n "${SSH_AGENT_PID}" ] && eval "$(ssh-agent)"
# synchronize mail with cron
#test -z $(pgrep -u $UID scrond) && scrond -f ~/.crontab

export PATH=$PATH:~/.local/bin:~/.go/bin:~/.cargo/bin:~/.cabal/bin/

export TERMINAL=st
export EDITOR=vim
export GUI_EDITOR=emacs
export GUI_BROWSER=firefox

export RUST_SRC_PATH=~/.local/src/rust/rust-1.37.0/
export GOPATH=$HOME/go
export JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk
