#!/bin/sh
# MPD daemon start (if no other user instance exists)
. /etc/profile
[ ! -f "$HOME/.config/mpd/pid" ] && mpd
# start ssh-agent if not already started
[ -z "${SSH_AGENT_PID}" ] && eval "$(ssh-agent)"
# synchronize mail with cron
#test -z $(pgrep -u $UID scrond) && scrond -f ~/.crontab

export PATH=~/.local/bin:~/.cargo/bin:~/.cabal/bin/:$PATH

export TERMINAL=st
export EDITOR=vim
export GUI_EDITOR=emacs
export GUI_BROWSER=firefox

export RUST_SRC_PATH=~/.local/src/rust/rust-1.37.0/
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$PATH
export JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk

export XDG_DATA_DIRS="$XDG_DATA_DIRS:/var/lib/flatpak/exports/share:/home/striker/.local/share/flatpak/exports/share"
export QT_QPA_PLATFORMTHEME="qt5ct"
