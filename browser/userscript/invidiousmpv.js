// ==UserScript==
// @name        Invidious MPV 
// @namespace   Violentmonkey Scripts
// @match       *://invidio.us/watch?v=*
// @match       *://yewtu.be/watch?v=*
// @match       *://invidious.13ad.de/watch?v=*
// @match       *://invidious.fdn.fr/watch?v=*
// @match       *://watch.nettohikari.com/watch?v=*
// @grant       none
// @version     1.0
// @author      striker.sh
// @description 6/5/2020, 2:04:26 PM
// @license     WTFPL
// ==/UserScript==
watchOnYoutube = document.getElementById('watch-on-youtube')
url = watchOnYoutube.children[0].getAttribute("href")
appLink = 'invidiapp:' + url
watchOnYoutube.insertAdjacentHTML("beforebegin", "<p id='watch-with-mpv'><a href=\"" + appLink + "\">Watch with MPV</a></p>")
