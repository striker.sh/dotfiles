#!/bin/sh
WORK_DIR="$( cd "$(dirname "$0")" && pwd -P )"
install(){
  CURR_DIR=$PWD
  cd "$WORK_DIR" || exit
  stow neovim bash mutt doom-emacs\
  trackma X11 bspwm rtv dunst mpd \
  newsboat dmenu-script wm-utilities zsh\
  trackma zathura sxiv conky polybar\
  fontconfig tmux qutebrowser env \
  mpv nnn

  cd ./surf/ && stow config
  cd ..

  neovim_setup
  emacs_setup
  slim_setup
  grub_setup
  x11_setup
  gen_colors.sh
  cd "$CURR_DIR" || exit
}

slim_setup(){
  if confirm "Do you want to install slim theme ?"; then
    sudo su -lc "
    test ! -d /usr/share/slim/themes && mkdir -p /usr/share/slim/themes
    if test ! -d /usr/share/slim/themes/thinkpad-theme; then
      cp -r $PWD/SLiM/thinkpad-theme /usr/share/slim/themes/thinkpad-theme
    fi
    if test -f /etc/slim.conf; then
      sed -i 's/\(current_theme \+\).*/\1thinkpad-theme/' /etc/slim.conf
    fi"
  fi
}

grub_setup(){
  if confirm "Do you want to install grub theme ?"; then
    sudo su -lc "
    if test ! -f /boot/background.png; then
      cp ./grub2/background.png /boot/
    fi
    "
  fi
}

x11_setup(){
  if confirm "Do you want to install X11 configuration (keyboard + libinput) ?"; then
    sudo xkalamine install "$WORK_DIR/Xorg/lafayette.yaml"
    sudo cp "$WORK_DIR/Xorg/*.conf /etc/X11/xorg.conf.d"
  fi 
}

neovim_setup(){
  if test ! -f ~/.local/share/nvim/site/autoload/plug.vim; then
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  fi
  if test ! -f ~/.config/nvim/colors/noctu.vim; then
    curl -fLo ~/.config/nvim/colors/noctu.vim --create-dirs \
      https://raw.githubusercontent.com/noahfrederick/vim-noctu/master/colors/noctu.vim
  fi
}

emacs_setup(){
  if test ! -d ~/.emacs.d; then
    CURR_DIR=$PWD
    git clone https://github.com/hlissner/doom-emacs ~/.emacs.d || return
    cd ~/.emacs.d || return
    git checkout develop
    ./bin/doom install
    cd "$CURR_DIR" || return
  fi
}

confirm(){
  while true; do
    printf "%s [y/N] " "$*"
    read -r answer
    case $answer in
      [Yy]* ) return 0;;
      [Nn]* ) return 1;;
      '' ) return 1;;
      * ) echo "Please answer yes or no.";;
    esac
  done
}

install
