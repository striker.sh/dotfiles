import sys, os

#sys.path.append(os.path.join(sys.path[0], 'jmatrix'))
#config.source("jmatrix/jmatrix/integrations/qutebrowser.py")
config.load_autoconfig()
config.source("configs/redirectors.py")
config.source("configs/appearance.py")


c.content.headers.user_agent = "Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0"
c.content.canvas_reading = False
c.content.cookies.store = False
c.content.cookies.accept = "no-3rdparty"
c.content.headers.accept_language="en-US,en;q=0.5"
c.content.headers.do_not_track = True
c.content.headers.custom= {"global": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
c.content.webgl=False
c.spellcheck.languages = ["fr-FR", "en-US"]
c.qt.args=["blink-settings=darkMode=4"]
c.content.default_encoding="utf-8"

# ablocking

c.content.blocking.method = 'adblock'

c.content.blocking.adblock.lists = [
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt",
        "https://easylist.to/easylist/fanboy-annoyance.txt",
        "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt",
        "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/annoyances.txt",
        "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt",
        "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt",
        "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/resource-abuse.txt",
        "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/unbreak.txt",
        "https://www.malwaredomainlist.com/hostslist/hosts.txt",
        "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=1&mimetype=plaintext",
        "https://www.i-dont-care-about-cookies.eu/abp/",
        "https://easylist-downloads.adblockplus.org/antiadblockfilters.txt"
    ]

config.bind(',v', 'spawn mpv {url}')
config.bind(',V', 'hint links spawn mpv {hint-url}')

c.editor.command = ['st', '-e', 'nvim', '{file}']
c.scrolling.smooth = True
c.content.dns_prefetch = True

