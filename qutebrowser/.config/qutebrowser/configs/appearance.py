import subprocess
def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props

c.fonts.contextmenu = "12px Monospace"
c.fonts.prompts = "12px Monospace"
c.fonts.statusbar = "12px Monospace"
c.fonts.completion.entry = "12px Monospace"
c.fonts.tabs.selected = "12px Monospace"
c.fonts.tabs.unselected = "12px Monospace"
c.fonts.debug_console = "12px Monospace"
c.fonts.completion.category = "bold 12px Monospace"
c.fonts.messages.error = "12px Monospace"
c.fonts.messages.info = "12px Monospace"
c.fonts.messages.warning = "12px Monospace"
c.fonts.downloads = "12px Monospace"

xressources = read_xresources('*')
# Background color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.bg = xressources['*background']

# Bottom border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.bottom = xressources['*background']

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.top = xressources['*background']

# Foreground color of completion widget category headers.
# Type: QtColor
c.colors.completion.category.fg = xressources['*foreground']

# Background color of the completion widget for even rows.
# Type: QssColor
c.colors.completion.even.bg = xressources['*background']

# Background color of the completion widget for odd rows.
# Type: QssColor
c.colors.completion.odd.bg = xressources['*background']

# Text color of the completion widget.
# Type: QtColor
c.colors.completion.fg = xressources['*foreground']

# Background color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.bg = xressources['*color10']

# Bottom border color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.border.bottom = xressources['*background']

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.item.selected.border.top = xressources['*background']

# Foreground color of the selected completion item.
# Type: QtColor
c.colors.completion.item.selected.fg = xressources['*color15']

# Foreground color of the matched text in the completion.
# Type: QssColor
c.colors.completion.match.fg = xressources['*color3']

# Color of the scrollbar in completion view
# Type: QssColor
c.colors.completion.scrollbar.bg = xressources['*background']

# Color of the scrollbar handle in completion view.
# Type: QssColor
c.colors.completion.scrollbar.fg = xressources['*foreground']

# Background color for the download bar.
# Type: QssColor
c.colors.downloads.bar.bg = xressources['*background']

# Background color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.bg = xressources['*color5']

# Foreground color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.fg = xressources['*foreground']

# Color gradient stop for download backgrounds.
# Type: QtColor
c.colors.downloads.stop.bg = xressources['*color6']

# Color gradient interpolation system for download backgrounds.
# Type: ColorSystem
# Valid values:
#   - rgb: Interpolate in the RGB color system.
#   - hsv: Interpolate in the HSV color system.
#   - hsl: Interpolate in the HSL color system.
#   - none: Don't show a gradient.
c.colors.downloads.system.bg = 'none'

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
# Type: QssColor
c.colors.hints.bg = xressources['*color9']

# Font color for hints.
# Type: QssColor
c.colors.hints.fg = xressources['*color15']

# Font color for the matched part of hints.
# Type: QssColor
c.colors.hints.match.fg = xressources['*color10']

# Background color of the keyhint widget.
# Type: QssColor
c.colors.keyhint.bg = xressources['*background']

# Text color for the keyhint widget.
# Type: QssColor
c.colors.keyhint.fg = xressources['*color15']

# Highlight color for keys to complete the current keychain.
# Type: QssColor
c.colors.keyhint.suffix.fg = xressources['*color3']

# Background color of an error message.
# Type: QssColor
c.colors.messages.error.bg = xressources['*color5']

# Border color of an error message.
# Type: QssColor
c.colors.messages.error.border = xressources['*color5']

# Foreground color of an error message.
# Type: QssColor
c.colors.messages.error.fg = xressources['*foreground']

# Background color of an info message.
# Type: QssColor
c.colors.messages.info.bg = xressources['*color10']

# Border color of an info message.
# Type: QssColor
c.colors.messages.info.border = xressources['*color10']

# Foreground color an info message.
# Type: QssColor
c.colors.messages.info.fg = xressources['*foreground']

# Background color of a warning message.
# Type: QssColor
c.colors.messages.warning.bg = xressources['*color1']

# Border color of a warning message.
# Type: QssColor
c.colors.messages.warning.border = xressources['*color1']

# Foreground color a warning message.
# Type: QssColor
c.colors.messages.warning.fg = xressources['*foreground']

# Background color for prompts.
# Type: QssColor
c.colors.prompts.bg = xressources['*background']

# # Border used around UI elements in prompts.
# # Type: String
c.colors.prompts.border = '1px solid ' + xressources['*background']

# Foreground color for prompts.
# Type: QssColor
c.colors.prompts.fg = xressources['*foreground']

# Background color for the selected item in filename prompts.
# Type: QssColor
c.colors.prompts.selected.bg = xressources['*color5']

# Background color of the statusbar in caret mode.
# Type: QssColor
c.colors.statusbar.caret.bg = xressources['*color6']

# Foreground color of the statusbar in caret mode.
# Type: QssColor
c.colors.statusbar.caret.fg = xressources['*cursorColor']

# Background color of the statusbar in caret mode with a selection.
# Type: QssColor
c.colors.statusbar.caret.selection.bg = xressources['*color6']

# Foreground color of the statusbar in caret mode with a selection.
# Type: QssColor
c.colors.statusbar.caret.selection.fg = xressources['*foreground']

# Background color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.bg = xressources['*background']

# Foreground color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.fg = xressources['*foreground']

# Background color of the statusbar in private browsing + command mode.
# Type: QssColor
c.colors.statusbar.command.private.bg = xressources['*background']

# Foreground color of the statusbar in private browsing + command mode.
# Type: QssColor
c.colors.statusbar.command.private.fg = xressources['*foreground']

# Background color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.bg = xressources['*color2']

# Foreground color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.fg = xressources['*background']

# Background color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.bg = xressources['*background']

# Foreground color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.fg = xressources['*foreground']

# Background color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.bg = xressources['*color10']

# Foreground color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.fg = xressources['*foreground']

# Background color of the statusbar in private browsing mode.
# Type: QssColor
c.colors.statusbar.private.bg = xressources['*background']

# Foreground color of the statusbar in private browsing mode.
# Type: QssColor
c.colors.statusbar.private.fg = xressources['*foreground']

# Background color of the progress bar.
# Type: QssColor
c.colors.statusbar.progress.bg = xressources['*foreground']

# Foreground color of the URL in the statusbar on error.
# Type: QssColor
c.colors.statusbar.url.error.fg = xressources['*color5']

# Default foreground color of the URL in the statusbar.
# Type: QssColor
c.colors.statusbar.url.fg = xressources['*foreground']

# Foreground color of the URL in the statusbar for hovered links.
# Type: QssColor
c.colors.statusbar.url.hover.fg = xressources['*color10']

# Foreground color of the URL in the statusbar on successful load
# (http).
# Type: QssColor
c.colors.statusbar.url.success.http.fg = xressources['*foreground']

# Foreground color of the URL in the statusbar on successful load
# (https).
# Type: QssColor
c.colors.statusbar.url.success.https.fg = xressources['*color2']

# Foreground color of the URL in the statusbar when there's a warning.
# Type: QssColor
c.colors.statusbar.url.warn.fg = xressources['*color1']

# Background color of the tab bar.
# Type: QtColor
c.colors.tabs.bar.bg = xressources['*background']

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = xressources['*background']

# Foreground color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.fg = xressources['*foreground']

# Color for the tab indicator on errors.
# Type: QtColor
c.colors.tabs.indicator.error = xressources['*color5']

# Color gradient start for the tab indicator.
# Type: QtColor
# c.colors.tabs.indicator.start = xressources['*color5']

# Color gradient end for the tab indicator.
# Type: QtColor
# c.colors.tabs.indicator.stop = xressources['*color1']

# Color gradient interpolation system for the tab indicator.
# Type: ColorSystem
# Valid values:
#   - rgb: Interpolate in the RGB color system.
#   - hsv: Interpolate in the HSV color system.
#   - hsl: Interpolate in the HSL color system.
#   - none: Don't show a gradient.
c.colors.tabs.indicator.system = 'none'

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = xressources['*background']

# Foreground color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.fg = xressources['*foreground']

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = xressources['*color10']

# Foreground color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.fg = xressources['*color15']

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = xressources['*color10']

# Foreground color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.fg = xressources['*foreground']

# Background color for webpages if unset (or empty to use the theme's
# color)
# Type: QtColor
c.colors.webpage.bg = xressources['*background']
