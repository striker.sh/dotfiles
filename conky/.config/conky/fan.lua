function fan(cr, line_spacing)
  local fan_speed = conky_parse("${exec cat /proc/acpi/ibm/fan |" ..
  " grep '^speed' | awk '{print $2}'}")
  local fan_speed_text = fan_speed .. " RPM"

  local top = {x = 75, y = 400}
  local bottom = {x = 115, y = 20}

  group(cr, top, bottom, "FAN")


  local lines = {
    'SPEED: ' .. fan_speed_text
  }

  display_lines(cr, lines, top.x, top.y, line_spacing)

end
