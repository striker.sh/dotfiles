function layout(cr, line_spacing)
  local top = {x = 75, y = 115}
  local bottom = {x = 90, y = 20}

  group(cr, top, bottom, "KEYBOARD")

  lines = {
    'LAYOUT: ' .. string.upper(conky_parse("${exec xkb-switch}"))
  }
  display_lines(cr, lines, top.x, top.y, line_spacing)
end
