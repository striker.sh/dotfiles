function cpu(cr, line_spacing)
  local cpu_perc=conky_parse("${cpu}")
  local cpu_temp = conky_parse("${acpitemp}")
  local usage_text = "USAGE: " .. cpu_perc .. "%"
  local temp_text  = "TEMP:  " .. cpu_temp .. "°C"
  local freq_text  = "FREQ:  " .. conky_parse("${freq}") .. "MHz"

  local lines = {
    usage_text,
    temp_text,
    freq_text
  }

  local top = {x = 75, y = 450}
  local bottom = {x = 110, y = 40}

  group(cr, top, bottom, "CPU")
  display_lines(cr, lines, top.x, top.y, line_spacing)
end
