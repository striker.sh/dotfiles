function wireless(cr, line_spacing)
  local top = {x = 645, y = 350}
  local bottom = {x = 150, y=40}
  group(cr, top, bottom, "WIRELESS")

  local ssid = conky_parse("${wireless_essid wlp3s0}")
  local lines = {
    "SSID:     " .. ssid,
  }
  if ssid ~= 'off/any' then
    local  optional = {
      "FREQ:     " .. conky_parse("${wireless_freq wlp3s0}"):gsub(',', '.'),
      "STRENGTH: " .. conky_parse("${wireless_link_qual_perc wlp3s0}") .. "%"
    }
    local lines = table_merge(lines, optional)
  end

  display_lines(cr, lines, top.x, top.y, line_spacing)
end
