function mem(cr, line_spacing)
  local used = conky_parse("${mem}"):gsub(',', '.')
  local total = conky_parse("${memmax}"):gsub(',', '.')
  local perc = conky_parse("${memperc}"):gsub(',', '.')

  local top = {x = 75, y = 620}
  local bottom = {x = 155, y=30}

  local lines = {
    'USED: ' .. used .. '/' .. total,
    'PERC: ' .. perc .. '%'
  }

  group(cr, top, bottom, "MEMORY")
  display_lines(cr, lines, top.x, top.y, line_spacing)

end
