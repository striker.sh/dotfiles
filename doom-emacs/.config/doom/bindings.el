;;; ~/.doom.d/binding.el -*- lexical-binding: t; -*-
(map!
 (:map override
   :n "U" #'redo
   )
 )

(setq evil-escape-key-sequence "kj")
(setq doom-localleader-key "\\")
(define-key! 'motion "\\" nil)
